from sklearn.datasets import fetch_mldata
from sklearn.linear_model import SGDClassifier
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


mnist = fetch_mldata('MNIST original')

X, y = mnist["data"], mnist["target"]
X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]
shuffle_index = np.random.permutation(60000)

some_digit = X[36000]
some_digit_image = some_digit.reshape(28,28)
plt.imshow(some_digit_image, cmap = matplotlib.cm.binary,
					interpolation="nearest")
plt.axis("off")
# plt.show()

y_train_5 = (y_train == 5) # True for all 5s, False for all other digits
y_test_5 = (y_test == 5)



sgd_clf = SGDClassifier(random_state=42)
# sgd_clf.fit(X_train, y_train_5)
# sgd_clf.predict([some_digit])


# Measuring Accuracy Using Cross-Validation
"""
	Occasionally, you will need more control over the cross-validation process than what Scikit-learn provides off-the-shelf. In these cases, you can implement 
	a cross-validation yourself; it is fairly straightforward. The following code does roughly the same thing as Scikit-Learn's cross_val_score() function, and prints the same result
"""

from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone

skfolds = StratifiedKFold(n_splits=3, random_state=42)

"""
	The StratifiedKFold class performs stratified sampling to produce folds that contain a representative ratio of each class. At each iteration the code creates a clone of the classifier,
	trains that clone on the training folds, and makes the predictions on the tet fold. Then it counts the number of correct predictions and outputs the ratio of correct predictions.

for train_index, test_index in skfolds.split(X_train, y_train_5):
	clone_clf = clone(sgd_clf)
	X_train_folds = X_train[train_index]
	y_train_folds = (y_train_5[train_index])
	X_test_fold = X_train[test_index]
	y_test_fold = (y_train_5[test_index])
	
	clone_clf.fit(X_train_folds, y_train_folds)
	y_pred = clone_clf.predict(X_test_fold)
	n_correct = sum(y_pred == y_test_fold)
	print(n_correct / len(y_pred))
"""	
	
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import confusion_matrix

cross = cross_val_score(sgd_clf, X_train, y_train_5, cv=3, scoring="accuracy")
y_train_pred = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3)
print(confusion_matrix(y_train_5, y_train_pred))
