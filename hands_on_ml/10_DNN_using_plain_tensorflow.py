from os import getcwd, path
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/")

# Tensorflow comes with many handy functions to create standard neural network layers, so there's often no need to defined
# my own like what I just did. 
def neuron_layer(X, n_neurons, name, activation=None):
	with tf.name_scope(name):
		n_inputs = int(X.get_shape()[1]) # Look up matrix shape and getting size of 2nd dimension. 1st dim is num_instances
		stddev = 2 / np.sqrt(n_inputs) # Weights matrix containing all conection weights between each input and each neuron -->
												  # size=n_inputs*n_neurons. This STDDEV is a small tweak that makes it more efficient.
		init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)	# All weights are randomized using Gaussian
		W = tf.Variable(init, name="kernel")
		b = tf.Variable(tf.zeros([n_neurons]), name="bias") # bias vector initialized to 0, with one bias parameter per neuron
		Z = tf.matmul(X, W) + b
		if activation is not None:
			return activation(Z)
		else:
			return Z

if __name__ == "__main__":
	
	n_inputs = 28*28 # MNIST
	n_hidden1 = 300
	n_hidden2 = 100
	n_outputs = 10 # Digits 0 - 9
	
	'''
		We can use placeholder nodes to represent the training data and targets. The shape of X is only partially defined.
		We know that it will be a 2D tensor, with instances along the first dimension and features along the second, and 
		we know that the number of features will be 28x28 (one feature per pixel), but we don't know yet how many instances
		each training batch will contain. 
			
		Similarly, y will be a 1D tensor with one entry per instance, but we don't know the size of the training batch at this point
	'''
	X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
	y = tf.placeholder(tf.int64, shape=(None), name="y")
	
	# Let's create the actual deep neural network
	with tf.name_scope("dnn"):
	
		# We use the name scope for clarity. logits is the output BEFORE going into the softmax layer for optimization reasons
		hidden1 = neuron_layer(X, n_hidden1, name="hidden1", activation=tf.nn.relu)
		hidden2 = neuron_layer(hidden1, n_hidden2, name="hidden2", activation=tf.nn.relu)
		logits = neuron_layer(hidden2, n_outputs, name="outputs")
		# hidden1 = tf.layers.dense(X, n_hidden1, name="hidden1", activation=tf.nn.relu) # Tensorflow equivalent
		# hidden2 = tf.layers.dense(hidden1, n_hidden2, name="hidden2", activation=tf.nn.relu) # Tensorflow equivalent
		# logits = tf.layers.dense(hidden2, n_outputs, name="outputs")
		
	# Neural network is ready to go. Now we need to define a cost function and use it to train the network
	# We will use sparse_softmax_cross_entropy_with_logits() -- it computes the cross entropy based on the "logits" i.e. the 
	# 		output before going through the softmax activation function
	# This gives us a 1D tensor containing the cross entropy for each instance and use Tensorflow's reduce_mean() to compute the 
	#  mean cross entropy over all instances
	
	with tf.name_scope("loss"):
		xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
		loss = tf.reduce_mean(xentropy, name="loss")
	
	# Now we need a GradientDescentOptimizer that will tweak the model parameters to minimize the cost function
	learning_rate = 0.01
	with tf.name_scope("train"):
		optimizer = tf.train.GradientDescentOptimizer(learning_rate)
		training_op = optimizer.minimize(loss)

	# Need to specify how to evaluate the model. Simply use accuracy as our performance measure.
	# First, for each instance, determine if NN prediction is correct by checking whether or not the highest logit corresponds to the target class.
	# This returns a 1D tensor full of bool vals, so we cast these bools to floats then compute the average to get the overall accuracy.
	with tf.name_scope("eval"):
		correct = tf.nn.in_top_k(logits, y, 1)
		accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
		
	# As per usual for Tensorflow; We need to create a node to initialize all variables and we will also create a Saver to save our trained
	# model parameters to disk:
	init = tf.global_variables_initializer()
	saver = tf.train.Saver()
	
	# Phew! Now this is the execution phase
	n_epochs = 40
	batch_size = 50
	
	# Open a session, run init node to initialize all Variables. Run the training loop: at each epoch, the code iterates through a number of mini-batches
	# that corresponds to the training set size. Next, at the end of each epoch, the code evaluates the model on the last mini-batch and on full test set,
	# printing the result. Finally, model params are saved to disk.
	with tf.Session() as sess:
		init.run()
		for epoch in range(n_epochs):
			for iteration in range(mnist.train.num_examples // batch_size):
				X_batch, y_batch = mnist.train.next_batch(batch_size)
				sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
			acc_train = accuracy.eval(feed_dict={X: X_batch, y: y_batch})
			acc_test = accuracy.eval(feed_dict={X: mnist.test.images,
																 y: mnist.test.labels})
			
			print(epoch, "Train accuracy: ", acc_train, "Test accuracy: ", acc_test)
			
		save_path = saver.save(sess, path.join(getcwd(), "my_model_final.ckpt"))

	"""
		Now that the neural network is trained, you can use it to make predictions. To do that, reuse the same construction phase, but change the execution phase
		 with the following code:
		 
		with tf.Session() as sess:
			saver.restore(sess, path.join(getcwd(), "my_model_final.ckpt"))
			X_new_scaled = [...] # Some new images (scaled from 0 to 1_)
			Z = logits.eval(feed_dict={X: X_new_scaled})
			y_pred = np.argmax(Z, axis=1)
	"""