import tensorflow as tf

import numpy as np
from sklearn.datasets import fetch_california_housing

housing = fetch_california_housing()
m, n = housing.data.shape
housing_data_plus_bias = np.c_[np.ones((m,1)), housing.data]

X = tf.constant(housing_data_plus_bias, dtype=tf.float32, name="X")

# Note that housing.target is a 1D array, but we need to reshape it to a column vector to compute theta
# Recall that NumPy's reshape() function accepts -1 (meaning "unspecified") for one of the dimensions: 
#		that dimension will be computed based on teh array's length and the remaining dimensions
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name="y")

XT = tf.transpose(X)
theta = tf.matmul(tf.matmul(tf.matrix_inverse(tf.matmul(XT, X)), XT), y)



# Tensorflow version of 04_normal_equation.py
X_2 = 2 * np.random.rand(100,1)
y_2 = 4 + 3 * X_2 + np.random.rand(100,1)

X_2_plus_bias = np.c_[np.ones((100,1)), X_2]
X_2_plus_bias_tf = tf.constant(X_2_plus_bias, dtype=tf.float32, name="X_2_bias_tf")

y_2_tf = tf.constant(y_2, dtype=tf.float32, name="y_2_tf")

XT_2 = tf.transpose(X_2_plus_bias_tf)

theta_best = tf.matmul(tf.matmul(tf.matrix_inverse(tf.matmul(XT_2, X_2_plus_bias_tf)), XT_2), y_2_tf)

with tf.Session() as sess:
	theta_value_best = theta_best.eval()
	theta_value = theta.eval()	

print("Theta value for housing: ")
print(theta_value)
print("\nTheta value for 4 + 3*X : ")
print(theta_value_best)
