import numpy as np
import tensorflow as tf
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import StandardScaler

housing = fetch_california_housing()

scaler = StandardScaler()
scaled_housing_data = scaler.fit_transform(housing.data)

m, n = housing.data.shape
scaled_housing_data_plus_bias = np.c_[np.ones((m,1)), scaled_housing_data]

# n_epochs = 1000
n_epochs = 100
learning_rate = 0.01

# X = tf.constant(scaled_housing_data_plus_bias, dtype=tf.float32, name="X")
X = tf.placeholder(tf.float32, shape=(None, n+ 1), name="X")
# y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name="y")
y = tf.placeholder(tf.float32, shape=(None, 1), name="y")

batch_size = 100
m = 1000
n_batches = int(np.ceil(m / batch_size))

theta = tf.Variable(tf.random_uniform([n + 1, 1], -1.0, 1.0, seed=42), name="theta")
y_pred = tf.matmul(X, theta, name="predictions")
error = y_pred - y
mse = tf.reduce_mean(tf.square(error), name="mse")
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
# gradients = 2/m * tf.matmul(tf.transpose(X), error)
# training_op = tf.assign(theta, theta - learning_rate * gradients)
training_op = optimizer.minimize(mse)

init = tf.global_variables_initializer()

def fetch_batch(epoch, batch_index, batch_size):
	np.random.seed(epoch * n_batches + batch_index)
	indices = np.random.randint(m, size=batch_size)
	X_batch = scaled_housing_data_plus_bias[indices]
	y_batch = housing.target.reshape(-1, 1)[indices]
	return X_batch, y_batch

with tf.Session() as sess:
	sess.run(init)
	
	for epoch in range(n_epochs):
		for batch_index in range(n_batches):
			X_batch, y_batch = fetch_batch(epoch, batch_index, batch_size)
			sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
		
	best_theta = theta.eval()
	
print(best_theta)