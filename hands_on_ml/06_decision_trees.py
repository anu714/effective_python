import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.datasets import load_iris, make_moons
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz

iris = load_iris()

def plot_decision_boundary(clf, X, y, axes=[0, 7.5, 0, 3], iris=True, legend=False, plot_training=True):
    x1s = np.linspace(axes[0], axes[1], 100)
    x2s = np.linspace(axes[2], axes[3], 100)
    x1, x2 = np.meshgrid(x1s, x2s)
    X_new = np.c_[x1.ravel(), x2.ravel()]
    y_pred = clf.predict(X_new).reshape(x1.shape)
    custom_cmap = ListedColormap(['#fafab0','#9898ff','#a0faa0'])
    plt.contourf(x1, x2, y_pred, alpha=0.3, cmap=custom_cmap, linewidth=10)
    if not iris:
        custom_cmap2 = ListedColormap(['#7d7d58','#4c4c7f','#507d50'])
        plt.contour(x1, x2, y_pred, cmap=custom_cmap2, alpha=0.8)
    if plot_training:
        plt.plot(X[:, 0][y==0], X[:, 1][y==0], "yo", label="Iris-Setosa")
        plt.plot(X[:, 0][y==1], X[:, 1][y==1], "bs", label="Iris-Versicolor")
        plt.plot(X[:, 0][y==2], X[:, 1][y==2], "g^", label="Iris-Virginica")
        plt.axis(axes)
    if iris:
        plt.xlabel("Petal length", fontsize=14)
        plt.ylabel("Petal width", fontsize=14)
    else:
        plt.xlabel(r"$x_1$", fontsize=18)
        plt.ylabel(r"$x_2$", fontsize=18, rotation=0)
    if legend:
        plt.legend(loc="lower right", fontsize=14)

def first_example():
	X = iris.data[:, 2:] # Petal length and width
	y = iris.target

	tree_clf = DecisionTreeClassifier(max_depth=2)
	tree_clf.fit(X, y)

	"""
	You can visualize the trained Decision Tree by first using the export_graphviz() method 
	"""
	export_graphviz(
		tree_clf,
		out_file="iris_tree.dot",
		feature_names=iris.feature_names[2:],
		class_names=iris.target_names,
		rounded=True,
		filled=True
		)

	
def nonparametric_decision_tree():
	Xm, ym = make_moons(n_samples=100, noise=0.25, random_state=53)
	
	deep_tree_clf1 = DecisionTreeClassifier(random_state=42)
	deep_tree_clf2 = DecisionTreeClassifier(min_samples_leaf=4, random_state=42)
	deep_tree_clf1.fit(Xm, ym)
	deep_tree_clf2.fit(Xm, ym)
	
	plt.figure(figsize=(11,4))
	plt.subplot(121)
	plot_decision_boundary(deep_tree_clf1, Xm, ym, axes=[-1.5, 2.5, -1, 1.5], iris=False)
	plt.title("No Restrictions", fontsize=16)
	plt.subplot(122)
	plot_decision_boundary(deep_tree_clf2, Xm, ym, axes=[-1.5, 2.5, -1, 1.5], iris=False)
	plt.title("min_samples_leaf = 4", fontsize=16)
	plt.show()
	

	
if __name__ == "__main__":
	#first_example()
	nonparametric_decision_tree()
