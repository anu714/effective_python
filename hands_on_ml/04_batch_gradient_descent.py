import numpy as np
import matplotlib.pyplot as plt

X = 2 * np.random.rand(100, 1)
y = 4 + 3 * X + np.random.rand(100, 1)

X_b = np.c_[np.ones((100,1)), X] # Add x0 = 1 to each instance

# Gradient Descent step
eta = 0.1 # learning rate
n_iterations = 1000
m = 100

theta = np.random.randn(2, 1) # Random initialization

for iteration in range(n_iterations):
	gradients = 2/m * X_b.T.dot(X_b.dot(theta) - y) # Gradient_theta_MSE(theta) = 2/m * X^T * (X * theta - y)
	theta = theta - eta * gradients
	

X_new = np.array([[0], [2]])
X_new_b = np.c_[np.ones((2,1)), X_new] # add x0 = 1 to each instance

# Stochastic Gradient Descent
n_epochs = 50
t0, t1 = 5, 50 # learning schedule hyperparameters

def learning_schedule(t):
	return t0/ (t + t1)
	
theta = np.random.rand(2,1) # Random initialization
p = 0
for epoch in range(n_epochs):
	for i in range(m):
		random_index = np.random.randint(m)
		xi = X_b[random_index:random_index + 1]
		yi = y[random_index:random_index + 1]
		gradients = 2 * xi.T.dot(xi.dot(theta) - yi)
		eta = learning_schedule(epoch * m + i)
		theta = theta - eta * gradients
		if p < 10:
			y_predict = X_new_b.dot(theta)
			plt.plot(X_new, y_predict, "r-")
			p += 1

y_predict = X_new_b.dot(theta)
plt.plot(X_new, y_predict, "g")
plt.plot(X, y, "b.")
plt.axis([0, 2, 0, 15])
plt.show()


