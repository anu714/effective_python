from math import inf
import pandas as pd
from sklearn.linear_model import LinearRegression
from parallel_ga.utils import *

ninf = -inf


class Population:
    """
    """
    def __init__(self, size, factors, mutation_rate=None, prior=0.3, model=LinearRegression):
        self.size = size  # Maximum size of the population
        self.factors = factors  # dimension of the problem, i.e. total num of variables
        self.mutation_rate = mutation_rate
        self.prior = prior
        self.evaluator = model
        self.population_count = 0  # Current number of members
        self.new_gen = None
        self.pop_df = None  # pandas DataFrame holding the binary codes representations of the models and scores
        self.survival_df = None

        if self.size % 2 != 0:
            self.size += 1

        if self.mutation_rate is None:  # Chance to mutation has not been defined, use general guideline for parameter
            self.mutation_rate = 1 / self.size

    def initialize_pool(self):
        """
        Initialize a random population of size self.factors. Using the prior probability, each member of the population
        should have, on average, 'prior' number of 1's and (1 - 'prior') number of 0's
        :return:
        """
        binary_codes = [pd.Series(np.random.binomial(1, self.prior, self.factors)) for _ in range(self.size)]
        codes_as_strings = [''.join(map(str, code)) for code in binary_codes]
        bool_codes = [convert_bin_to_bool(code) for code in binary_codes]
        self.pop_df = pd.DataFrame({'string_codes': codes_as_strings,
                                    'binary_codes': binary_codes,
                                    'bool_codes': bool_codes})
        self.pop_df['model_scores'] = ninf

    def _selection(self):
        while True:
            rand1 = np.random.choice(self.survival_df.axes[0])  # We want the axis that represents the indices
            rand2 = np.random.choice(self.survival_df.axes[0])
            if rand1 != rand2:
                break

        p1 = self.pop_df.iloc[rand1, :]
        p2 = self.pop_df.iloc[rand2, :]
        return p1['binary_codes'], p2['binary_codes']

    def _reproduction(self, parent_one, parent_two, mutate_active=True):
        """
        Two individuals are selected at random to produce a child. Typically, a cross-over position is chosen at
        random between 1 and p, say j; the child then inherits the first j genes from parent 1 and the rest p-j genes
        from parent 2.
        :param parent_one: Binary string representing a model that serves as one parent
        :param parent_two: Binary string representing another model that serves as another parent
        :return: a child that inherited genes from both parents
        """
        j = np.random.random_integers(1, self.factors)  # Get rand cross-over position using discrete uniform dist
        child = pd.concat([parent_one[:j], parent_two[j:]])
        if mutate_active:
            child = self._mutation(child)
        return child

    def _mutation(self, child):
        """
        At birth, an individual is allowed, with a certain small probability (called the mutation rate), to alter its
        genetic code at a randomly chosen position. In the typical setting where binary codes are used, this amounts
        to flipping a 0 to 1, or vice versa
        :param child:
        :return:
        """
        if self.mutation_rate > np.random.random():
            # [low, high] INCLUSIVE!!
            j = np.random.random_integers(0, self.factors - 1)  # Get rand position to mutate gene
            gene = child[j]
            mutated_gene = (gene + 1) % 2  # Assuming binary code
            child[j] = mutated_gene
        return child

    def update(self, survival_pool, **kwargs):
        """
        Public method that takes in a population of the 'fittest' to create a new generation of models up to population
         size defined by self.size.
        :param survival_pool: A list of
        :param kwargs: keyword arguments sent to the private reproduction method
        :return:
        """
        self.survival_df = survival_pool
        new_gen = {'string_codes': [],
                   'binary_codes': [],
                   'bool_codes': [],
                   'model_scores': []}  # survival_pool
        # self.new_gen.reset_index(drop=True, inplace=True)

        self.population_count = len(survival_pool)
        while self.population_count < self.size:
            p1, p2 = self._selection()
            child = self._reproduction(p1, p2, **kwargs)  # Child is a binary code
            child_string = convert_bin_to_str(child)
            child_bool = convert_bin_to_bool(child)
            # TODO: Inefficient
            new_gen['string_codes'].append(child_string)
            new_gen['binary_codes'].append(child)
            new_gen['bool_codes'].append(child_bool)
            new_gen['model_scores'].append(ninf)

            self.population_count += 1

        # Total size of the next generation is the same as the original, so set the new population to pop_df
        self.pop_df = self.survival_df.append(pd.DataFrame(new_gen), ignore_index=True)

    def get_top_performers(self, n=1):
        return self.pop_df.sort_values(by=['model_scores'], ascending=False).head(n)

    def get_random_binary_code(self):
        return pd.Series(np.random.binomial(1, self.prior, self.factors))  # Return random combinations of 0, 1
