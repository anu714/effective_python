"""
An Python implementation of Darwinian Evolution in Parallel Universes: A Parallel Genetic Algorithm for Variable Selection
 by Mu Zhu and Hugh A. Chipman.

Some function docstrings are excerpts from the Paper
:author Adam McConnell
:date 04/17/2018
"""
import math
import pandas as pd
from sklearn.linear_model import LinearRegression
from parallel_ga.Population import Population
from parallel_ga.utils import *
from concurrent.futures import ProcessPoolExecutor
from time import time


def create_synthetic_set(n, p, mu=0, sigma=1):
    """
    Returns X, n x p matrix of synthetic data with each column being an independent Gaussian distribution specified by
    mu, and sigma
    :param n: num of rows (observations)
    :param p: num of variables
    :param mu: center of Gaussian distribution
    :param sigma: spread of Gaussian distribution
    :return: a synthetic dataset
    """
    return pd.concat([pd.Series(np.random.normal(mu, sigma, n)) for _ in range(p)], axis=1)


def projection_matrix(X_w):
    """ X_w * ( X_w^T * X_w )^ -1 * X_w^T """
    return X_w.dot(np.linalg.inv(X_w.T.dot(X_w)).dot(X_w.T))


def cvrss(y_true, y_omega, h, generalize=False, q=0):
    """
    Fitness function for the Genetic Algorithm
    :param y_true: actual observations
    :param y_omega: predicted observations
    :param h: hat matrix: X_w * ( X_w^T * X_w )^ -1 * X_w^T
    :param generalize: bool, if set to True, then will use the GCVRSS instead
    :param q: if generalize, then q is the number of selected variables used in X_w dataset. |w_i| = q
    :return: leave-one-out cross-validated residual sum-of-squares fitness score
    """
    if generalize and q != 0:
        return -(((y_true[1:] - y_omega[1:]) / (q + 1)) ** 2).sum()
    else:
        return -(((y_true[1:] - y_omega[1:]) / (1 - h[1:])) ** 2).sum()


def single_path_genetic_algorithm(y_true, X, pop_size, num_generations,
                                  mutation_rate=None, prior=0.3, fitness_func=cvrss):
    """
    :param y_true: n x 1 vector
    :param X: n x p matrix
    :param pop_size: population size, generally the dimension size of the problem; if odd, then + 1
    :param num_generations: Number of generations to evolve
    :param mutation_rate: Mutation rate for generation t; default = 1 / pop_size for every t
    :param prior: prior probability used to initialize the GA; default = 0.3
    :param fitness_func: function used to score the model, default is leave-one-out CV RSS
    :return: The last generation population, P(N)
    """
    # y_true, X, pop_size, num_generations = some_args
    factors = len(X.columns)
    median = int(pop_size / 2)
    population = Population(pop_size, factors, mutation_rate=mutation_rate, prior=prior)
    population.initialize_pool()
    model = LinearRegression()

    bool_column = population.pop_df.columns.get_loc('bool_codes')
    score_column = population.pop_df.columns.get_loc('model_scores')

    t = 0
    while t < num_generations:
        population_df = population.pop_df
        # population_df.set_index('string_codes', inplace=True)
        #  For every w_i in P(t), find its fitness score s_i = F(w_i)
        for index in population_df.index:
            bool_code = population_df.iloc[index, bool_column]  # code, 'bool_codes']
            if np.any(bool_code):  # There needs to be at least one True s.t. x_w is not empty
                x_w = X[X.columns[bool_code]]
                h = projection_matrix(x_w)
                temp = model.fit(x_w, y_true)
                population_df.iloc[index, score_column] = fitness_func(y_true, model.predict(x_w), np.diagonal(h))

        # population_df.reset_index(inplace=True)
        # Let the 'survival pool', S(t) be the set of top m/2 members
        population_df.sort_values(by=['model_scores'], ascending=False, inplace=True)
        survival_pool = population_df.head(median)  # Get top m/2 individuals
        population.update(survival_pool)

        print(t)
        t += 1
    # print(population.get_top_performers())

    return population


def pga(B, num_workers, *args):
    """
    :param B: Number of parallel universes to spawn
    :param num_workers: Number of workers for parallel processing
    :param args: all required arguments for the SGA algorithm
    :return: In matrix form, r(j, b) for j = 1,2,...,p and b = 1,2,...,B
    """
    start = time()
    r_jb = []

    #  multiverse = [single_path_genetic_algorithm(*args) for _ in range(B)] # SINGLE-THREADED PROCESS
    with ProcessPoolExecutor(max_workers=num_workers) as pool:
        futures = [pool.submit(single_path_genetic_algorithm, *args) for _ in range(B)]

    end = time()
    print("Total time: %f" % (end - start))
    multiverse = [universe.result() for universe in futures]  # Grab all the Population data after futures processing

    for universe in multiverse:
        r_jb.append([r(j, universe) for j in range(universe.factors)])

    r_matrix = np.matrix(r_jb)
    p = len(r_jb[0])  # Number of variables or factors
    return r_matrix.ravel().reshape((B, p))


def r(j, b):
    """
    r(j, b) = 1/m * sum[ w_i (j) ]
    0 <= r(j, b) <= 1 as a measure of the importance for variable j based on N generations of evolution in universe b.
    In other words, for each universe b, r(j, b), is the percentage of last-generation candidates that contain var j
    :param j: the variable. w_i (j)
    :param b: universe, or the population after N generations
    :return: A measure of the importance for variable j based on N generations of evolution
    """
    sum = 0
    m = b.size
    binary_column = b.pop_df.columns.get_loc('binary_codes')
    for idx, row in b.pop_df.iterrows():
        sum += row.iloc[binary_column][j]
    return sum / m


def entropy(r):
    """
    Given a collection of binary sequences of length p, let r_j be the frequency that the jth bit is equal to 1.
    This calculates the average 'per bit' entropy of this collection
    :param r: binary sequence of length p
    :return: entropic value of the binary sequence
    """
    sum = 0
    p = len(r)
    for j in range(p):
        sum += r[j] * math.log2(r[j]) + (1 - r[j]) * math.log2(1 - r[j])
    return (-1/p) * sum


def main():
    n = 80  # Number of observations
    p = 20  # Number of variables
    mu, sigma = 0, 1  # Mean and Standard Deviation
    population_size = 25
    number_of_generations = 4
    B = 20  # Number of parallel paths
    workers = 6  # Number of workers for parallel tasks in ProcessPollExecutor
    error = np.random.normal(mu, sigma, n)

    synth_df = create_synthetic_set(n, p, mu, sigma)
    # y = 2 * synth_df.loc[:, 4] + 3 * synth_df.loc[:, 9] + 4 * synth_df.loc[:, 14]# + error
    y = 2 * synth_df.loc[:, 4] + 2 * synth_df.loc[:, 9] + error
    # result = single_path_genetic_algorithm(y, synth_df, population_size, number_of_generations)
    # r_s = [r(j, result) for j in range(p)]
    # print(r_s)

    # test = np.array(r_s)
    # print(np.mean(r_s), np.var(r_s))
    r_matrix = pga(B, workers, y, synth_df, population_size, number_of_generations)
    # print(r_matrix)
    r_j_avg = r_matrix.sum(axis=0, dtype='float') / B
    print('r_j_avg: ', r_j_avg[0])
    display_scatter(r_j_avg.T.tolist(), marker='x')


if __name__ == '__main__':
    main()
