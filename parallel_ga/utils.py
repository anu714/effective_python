import matplotlib.pyplot as plt
import numpy as np


def convert_bin_to_str(binary_code):
    return ''.join(map(str, binary_code))


def convert_bin_to_bool(bincode):
    bool_string = []
    for val in bincode:
        if val == 1:
            bool_string.append(True)
        else:
            bool_string.append(False)
    return bool_string


def display_histogram(s, mu, sigma):
    count, bins, _ = plt.hist(s, 30, normed=True)
    plt.plot(bins, 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(bins - mu) ** 2 / (2 * sigma ** 2)),
             linewidth=2, color='r')
    plt.show()


def display_scatter(some_list, **kwargs):
    xlabels = np.arange(1, len(some_list) + 1, 1)
    plt.scatter(xlabels, some_list, **kwargs)
    plt.xticks(xlabels)
    plt.show()


if __name__ == '__main__':
    print("This is a utility file, not a Python script or program.")
