
"""
There's a draw for Python programmers to give special meaning to the return value of None. It seems to make sense in some cases. 
For example, if you have a helper function that divides one number by another; in the case of dividing by zero, returning None seems natural
because the result is undefined.
"""
def divide(a, b):
	try:
			return a / b
	except ZeroDivisionError:
			return None