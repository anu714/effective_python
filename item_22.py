import collections
"""
Item 22: Prefer Helping Classes Over Bookkeeping with Dictionaries and Tuples
"""


class SimpleGradebook(object):
    """
    For example, say you want to record the grades of a set of students whose names aren't known in advance.
    Define a class to store names in a dictionary instead of using a predefined attribute for each student
    """
    def __init__(self):
        self._grades = {}

    def add_student(self, name):
        self._grades[name] = []

    def report_grade(self, name, score):
        self._grades[name].append(score)

    def average_grade(self, name):
        grades = self._grades[name]
        return sum(grades) / len(grades)


class BySubjectGradebook(object):
    """
    Dictionaries are so easy to use that there's a danger of overextending them to write brittle code. For example,
    say you want to extend the SimpleGradebook class to keep a list of grades by subject, not just overall.

    You can do this by changing the _grades dictionary to map student names (the keys) to another dictionary (the values).
    the innermost dictionary will map subjects (the keys) to grades (the values)
    """
    def __init__(self):
        self._grades = {}

    def add_student(self, name):
        self._grades[name] = {}

    """
    Straightforward enough, The next two methods will gain a bit of complexity to deal with the multilevel dictionary,
    but it's manageable
    """
    def report_grade(self, name, subject, grade):
        by_subject = self._grades[name]
        grade_list = by_subject.setdefault(subject, [])
        grade_list.append(grade)

    def average_grade(self, name):
        by_subject = self._grades[name]
        total, count = 0, 0
        for grades in by_subject.values():
            total += sum(grades)
            count += len(grades)
        return total / count


class WeightedGradebook(object):
    """
    Now imagine your requirements change again. You also want to track the weight of each score toward the overall
    grade in the class so midterms and finals are more important than pop quizzes. One way to implement this feature
    is to change the innermost dictionary; instead of mapping subjects (the keys) to grades (the values), Use the
    tuple (score, weight) as values.
    """
    def __init__(self):
        self._grades = {}

    def add_student(self, name):
        self._grades[name] = {}

    """
    Although the changes to report_grade seem simple, the average_grade method now has a loop within a loop and is 
    difficult to read.
    """
    def report_grade(self, name, subject, score, weight):
        by_subject = self._grades[name]
        grade_list = by_subject.setdefault(subject, [])
        grade_list.append((score, weight))

    def average_grade(self, name):
        by_subject = self._grades[name]
        score_sum, score_count = 0, 0
        for subject, scores in by_subject.items():
            subject_avg, total_weight = 0, 0
            for score, weight in scores:
                subject_avg += score * weight
                total_weight += weight
            score_sum += subject_avg / total_weight
            score_count += 1
        return score_sum / score_count


"""
The pattern of extending tuples longer and longer is similar to deepening layers of dictionaries. As soon as you find
yourself going longer than a two-tuple, it's time to consider another approach.

The namedtuple type in the collections module does exactly what you want. It lets you easily define tiny, immutable
data classes.

An important limitation of namedtuple is that the attribute values of namedtuple instances are still accessible by
numerical indexes and iteration. In externalized APIs, this can lead to unintentional usage that makes it harder
to move to a real class later. If you're not in control of all the usage of your namedtuple instances, it's better to
define your own class
"""
Grade = collections.namedtuple('Grade', ('score, weight'))


class Subject(object):
    """
    You can write a class to represent a single subject that contains a set of grades
    """
    def __init__(self):
        self._grades = []

    def report_grade(self, score, weight):
        self._grades.append(Grade(score, weight))

    def average_grade(self):
        total, total_weight = 0, 0
        for grade in self._grades:
            total += grade.score * grade.weight
            total_weight += grade.weight
        return total / total_weight


class Student(object):
    """
    Then you would write a class to represent a set of subjects that are being studied by a single student
    """
    def __init__(self):
        self._subjects = {}

    def subject(self, name):
        if name not in self._subjects:
            self._subjects[name] = Subject()
        return self._subjects[name]

    def average_grade(self):
        total, count = 0, 0
        for subject in self._subjects.values():
            total += subject.average_grade()
            count += 1
        return total / count


class Gradebook(object):
    """
    Finally, you would write a container for all the students keyed dynamically by their names
    """
    def __init__(self):
        self._students = {}

    def student(self, name):
        if name not in self._students:
            self._students[name] = Student()
        return self._students[name]


if __name__ == '__main__':
    book = SimpleGradebook()
    book.add_student('Isaac Newton')
    book.report_grade('Isaac Newton', 90)
    book.report_grade('Isaac Newton', 85)
    book.report_grade('Isaac Newton', 98)
    book.report_grade('Isaac Newton', 50)
    print("Student: %s, Grade: %d" % ('Isaac Newton', book.average_grade('Isaac Newton')))

    # Using this class remains simple
    book = BySubjectGradebook()
    book.add_student('Albert Einstein')
    book.report_grade('Albert Einstein', 'Math', 75)
    book.report_grade('Albert Einstein', 'Math', 95)
    book.report_grade('Albert Einstein', 'Gym', 55)
    book.report_grade('Albert Einstein', 'Gym', 65)
    print("Student: %s, Grade: %d" % ('Albert Einstein', book.average_grade('Albert Einstein')))

    wbook = WeightedGradebook()
    # It's unclear what all the numbers in the positional arguments mean. It's time to make the leap from dicts to
    # a hierarchy of classes
    wbook.add_student('Albert Einstein')
    wbook.report_grade('Albert Einstein', 'Math', 80, 0.1)

    """ The line count of these classes is almost double the previous implementation, but is much easier to read. """
    book = Gradebook()
    albert = book.student('Albert Einstein')
    math = albert.subject('Math')
    math.report_grade(75, 0.2)
    math.report_grade(95, 0.8)
    math.report_grade(55, 0.5)
    math.report_grade(65, 0.3)
    print(albert.average_grade())
